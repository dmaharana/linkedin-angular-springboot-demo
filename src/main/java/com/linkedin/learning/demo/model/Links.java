package com.linkedin.learning.demo.model;

public class Links {

	private Self self;
	

	public Links(Self self) {
		super();
		this.self = self;
	}

	public Self getSelf() {
		return self;
	}

	public void setSelf(Self self) {
		this.self = self;
	}
}
